package controller;

import java.awt.Window;

import javax.swing.JOptionPane;

import view.Windoww;
import model.Movie;
import model.Movies;
import model.dao.MovieDAO;

public class Main {
	private static Movies list = new Movies();	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Window movies = new Windoww();
		movies.setVisible(true);
		movies.setLocationRelativeTo(null);
	}
	
	public static void addMovie(Movie m) {
		list.addM(m);
		System.out.println("a");
	}
	
	public static String listMovies() {
		return list.toString();
	}
	
	public static void createMovie(String title, String genre, int year) {
		
		Movie n = new Movie(title, genre, year);
		
		MovieDAO.insertMovie(n);
		JOptionPane.showMessageDialog(null, "¡Saved in database!");
	}
	
}
