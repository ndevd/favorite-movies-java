package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import connection.Connect;
import model.Movie;

public class MovieDAO {
	//INSERT
	public static void insertMovie(Movie m) {
		Connect conn = new Connect();
		String str = "INSERT INTO movie(title, genre, year) values(" + "'"+ m.getTitle() +"'" + "," + "'"+ m.getGenre()+"'" + "," 
		+ m.getYear() + ");";
		
		try {
			Statement sentence = conn.getConnection().createStatement();
            sentence.executeUpdate(str);
            sentence.close();
            conn.disconnect();

		}catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "Error trying to connect, could not insert"
                    + "", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	//SELECT
	public static ArrayList<Movie> returnList() {
		ArrayList<Movie> list = new ArrayList<Movie>();
		Connect conn = new Connect();
		String str = "SELECT * FROM movie";
		
		try {
			PreparedStatement sentence = conn.getConnection().prepareStatement(str);
			ResultSet rs = sentence.executeQuery();
			while(rs.next()) {
				String title = rs.getString("title");
				String genre = rs.getString("genre");
				int year = rs.getInt("year");
				
				Movie aux = new Movie(title, genre, year);
				list.add(aux);
			}
			
			rs.close();
			sentence.close();
			conn.disconnect();
				
		}catch(SQLException ex) {
			JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE );
		}
		return list;
	}
	
}
