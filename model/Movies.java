package model;
import java.util.ArrayList;

public class Movies {
	private ArrayList<Movie> list;
	
	public Movies() {
		list = new ArrayList <Movie>();
	}
	
	public void addM(Movie m) {
		list.add(m);
	}

	@Override
	public String toString() {
		String str = "";
		for (Movie m : list) {
			str += m.toString() + "\n";
		}
		return str;
	}
	
}
