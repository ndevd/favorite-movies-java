package model;

public class Movie {
	//Attributes
	private String title;
	private String genre;
	private int year;
	
	//Constructor
	public Movie(String title, String genre, int year) {
		setTitle(title);
		setGenre(genre);
		setYear(year);
	}
	
	//Getters
	public String getTitle() {
		return title;
	}
	public String getGenre() {
		return genre;
	}
	public int getYear() {
		return year;
	}
	
	//Setters
	public void setTitle(String title) {
		this.title = title;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "- Title: " + title + ", Genre: " + genre + ", Year: " + year + " -";
	}
	
}
