package view;

//import java.awt.BorderLayout;

//import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import controller.Main;
import model.Movie;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class Windoww extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldTitle;
	private JTextField textFieldGenre;
	private JTextField textFieldYear;

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { Windoww frame = new Windoww();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */

	/**
	 * Create the frame.
	 */
	public Windoww() {
		setResizable(false);
		setTitle("Movies");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 612, 346);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelAdd = new JPanel();
		panelAdd.setBorder(new LineBorder(Color.BLACK));
		panelAdd.setBounds(12, 12, 295, 290);
		contentPane.add(panelAdd);
		panelAdd.setLayout(null);
		
		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(12, 9, 36, 15);
		panelAdd.add(lblTitle);
		
		textFieldTitle = new JTextField();
		textFieldTitle.setBounds(66, 7, 150, 19);
		panelAdd.add(textFieldTitle);
		textFieldTitle.setColumns(10);
		
		JLabel lblGenre = new JLabel("Genre:");
		lblGenre.setBounds(12, 40, 48, 15);
		panelAdd.add(lblGenre);
		
		textFieldGenre = new JTextField();
		textFieldGenre.setBounds(66, 38, 150, 19);
		panelAdd.add(textFieldGenre);
		textFieldGenre.setColumns(10);
		
		JLabel lblYear = new JLabel("Year:");
		lblYear.setBounds(12, 71, 37, 15);
		panelAdd.add(lblYear);
		
		textFieldYear = new JTextField();
		textFieldYear.setBounds(66, 69, 150, 19);
		panelAdd.add(textFieldYear);
		textFieldYear.setColumns(10);
		
		JTextArea textAreaAdd = new JTextArea();
		textAreaAdd.setEditable(false);
		textAreaAdd.setBounds(12, 98, 271, 178);
		panelAdd.add(textAreaAdd);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String title = textFieldTitle.getText();
				String genre = textFieldGenre.getText();
				String yeart = textFieldYear.getText();
				
				if(title.isBlank() ) {
					JOptionPane.showMessageDialog(contentPane, "Error");
				}else {
					int year = Integer.parseInt(yeart);
					Movie m = new Movie(title, genre, year);
					Main.addMovie(m); //Add to ArrayList
					textAreaAdd.setText(Main.listMovies());
					Main.createMovie(title, genre, year); //Add to Database
				}
			}
		});
		btnAdd.setBounds(222, 66, 61, 25);
		panelAdd.add(btnAdd);
		
		JPanel panelSearch = new JPanel();
		panelSearch.setBorder(new LineBorder(Color.BLACK));
		panelSearch.setBounds(305, 12, 295, 290);
		contentPane.add(panelSearch);
		panelSearch.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 271, 241);
		panelSearch.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		
		JButton btnList = new JButton("List");
		btnList.setBounds(223, 265, 60, 25);
		panelSearch.add(btnList);
	}
}
