package connection;

import java.sql.*;

import javax.swing.JOptionPane;

public class Connect {
	private static String db = "FavoriteMovies_DB";
	private static String login = "neodev";
	private static String password = "p455w0rd";
	private static String url = "jdbc:mariadb://localhost:3306/" + db;

	private Connection conn = null;

	public Connect() {

		try {
			//mariadb driver
			Class.forName("org.mariadb.jdbc.Driver");
			conn = DriverManager.getConnection(url, login, password);
			if (conn != null) {
				JOptionPane.showMessageDialog(null, "Conection OK\n");
			}
		} catch (SQLException e) {
			System.out.println(e);
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}

	}
	
	//Return connection
    public Connection getConnection(){
        return conn;
    }
    
    //Disconnect DB
    public void disconnect(){
        conn = null;
    }
}
